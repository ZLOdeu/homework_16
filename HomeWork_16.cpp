﻿
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime> 
using namespace std;

int main() 
{
    const int ar = 2;
    int array[ar][ar];

    for (int i = 0; i < ar; i++) 
    {
        for (int j = 0; j < ar; j++) 
        {
            array[i][j] = i + j;
        }
    }
    for (int i = 0; i < ar; i++) 
    {
        for (int j = 0; j < ar; j++) 
        {
            cout << array[i][j] << " ";
        }
        cout << endl;
    }
    
    setlocale(LC_ALL, "rus");
    int sum = 0;
    time_t t;
    time(&t);
    int k = (localtime(&t)->tm_mday) % ar;


    for (int i = 0; i < ar; i++)
        for (int j = 0; j < ar; j++)
        {
            cin >> array[i][j];
            if (i == k) sum += array[i][j];
        }
    cout << "Сумма элементов строки " << k << " = " << sum;

    return 0;
}

